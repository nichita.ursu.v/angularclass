import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/lessons/lessons.module').then(m => m.LessonsModule),
  },
  {
    path: 'auth',
    loadChildren: () => import('./features/auth/auth.module').then(m => m.AuthModule),
  }
];

export const appRouting: ModuleWithProviders<RouterModule> = RouterModule.forRoot(appRoutes);
