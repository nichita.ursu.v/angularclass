import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { authRouting } from './auth.routing';
import { AuthComponent } from './components/auth.component';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
  ],
  imports: [
    SharedModule,
    authRouting,
  ]
})
export class AuthModule {}
