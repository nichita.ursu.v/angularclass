import { NgModule } from '@angular/core';
import { LessonsComponent } from './components/lessons.component';
import { LessonsListComponent } from './components/lessons-list/lessons-list.component';
import { lessonsRouting } from './lessons.routing';

@NgModule({
  declarations: [
    LessonsComponent,
    LessonsListComponent,
  ],
  imports: [
    lessonsRouting,
  ]
})
export class LessonsModule {}
