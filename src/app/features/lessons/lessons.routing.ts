import { Routes, RouterModule } from '@angular/router';
import { LessonsComponent } from './components/lessons.component';
import { LessonsListComponent } from './components/lessons-list/lessons-list.component';
import { ModuleWithProviders } from '@angular/core';

const lessonsRoutes: Routes = [
  {
    path: '',
    component: LessonsComponent,
    children: [
      {
        path: '',
        component: LessonsListComponent,
      }
    ],
  },
];

export const lessonsRouting: ModuleWithProviders = RouterModule.forChild(lessonsRoutes);
